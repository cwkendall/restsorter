FROM openjdk:8-jdk-alpine
MAINTAINER Chris Kendall <ckendall@gmail.com>

ENV JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -Dvertx.disableDnsResolver=true -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory

EXPOSE 8080

VOLUME /tmp
ADD /target/*-fat.jar app.jar
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar /app.jar"]
