# REST sorter - array sorting micro-service

## Requirements

A simple RESTful service for sorting arrays.

The base requirements are:
`Objective: Implement a REST API to sort an Integer Array, among which there are no repetitive values and the max of the value is 1000, and min is 1.
1.  Please describe how you are going to deploy this API to some servers, and which kind of servers, so that users can access the API
2.  Please explain the Pros and Cons of the implementation and deployment, in terms of scalability, accessibility and reliability, etc.`

### Deployment

This application has been deployed as a containerized application (using Docker) onto a Kubernetes cluster.
The repository contains CI/CD scripts to complete a deployment with 3 replicas on Google Container Engine.
The goal here is to create a highly reliable and redundant service managed by the Kubernetes platform.

Auto-scaling and self-healing are supported by Kubernetes which makes it an excellent choice for web-scale applications
that follow 12-factor principles.

Note: clustering has not been implemented but would be a logical next step to ensure data consistency between node failures.
Also, the pod for each replica should be located in different geographical regions for greater levels of redundancy

In addition to the REST API, the MuleSoft API Console has been deployed for this application to enable developers to
easily understand and test out the API dynamically in the browser.  API first development practises have been followed here,
using RAML to both document the API and also to provide verification in the automated test suite.


### Implementation

Vert.x is the lightweight REST framework used to implement this API service running on the JVM.
Although this sample application only supports arrays up to 1000 elements in length, the event-based (reactive)
programming model of Vert.x (similar to node.js), means that the application has the potential to scale to much larger
request sizes and remain responsive even with many thousands of requests per second.

The service is designed with an 'arrays' REST resource which is intended to support a job-based request model for high-scalability.
This means that API consumers will 'POST' array data to the service and the sorting process is triggered either
by a followup GET request or asynchronously via a callback mechanism.

The GET request can be supplied with parameters to customise the presentation of the array including sort order and page size.

It was considered that with small arrays the sorted data could have been immediately returned in the response of the initial request,
however that solution does not correspond with RESTful principles and meant that:
1. Developers might not find the API intuitive despite its simplicity,
2. It may make the it difficult to extend the API without breaking it, and
3. the server would always block a request thread while waiting for the array to upload and for the sort to complete.
This is not ideal for scalability or extensibility

The implemented APIs provide a greater amount of flexibility for developers.
The sort action can occur synchronously during a subsequent GET request on an arrayId that has been previously 'stored'.
Alternatively, a callback API can notify the consumer when the sort operation has completed asynchronously.
By supplying an additional parameter to the POST request e.g. " sort: { callback: <my-server>, order: asc } ",
the application will POST to the callback URL once the sort operation is completed.
Under a high request load or larger array sizes, using the callback API model scales better as request threads are be
short-lived even for long-running sort operations.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You will need Maven 3 and Java 8 to build this project.

Optionally, Docker CLI tools can be used to build from the `Dockerfile`.
This is not currently integrated into the Maven build and instead relies on Gitlab CI

GKE / Kubernetes are used for production deploys

### Getting setup
```
# MacOS
# Install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install maven
brew cask install java

# Ubuntu/Debian Linux
apt-get install -y --no-install-recommends openjdk-8-jre-headless maven
```

### Installing

Clone the repository
```
git clone https://gitlab.com/cwkendall/rest-sorter.git
```

Building the application using maven (no profile required). This will produce both a fat and skinny JAR.
```
mvn install
```

If a skinny (regular) jar is used, then dependencies should be copied to the output folder and loaded onto the classpath
during runtime e.g. after building:
```
mvn dependency:copy-dependencies
java -jar target/<skinny.jar> -cp target/dependency/*.jar
```

Building the docker image (optional)
```
docker build -t <appname>:<label>
```

## Running the tests

Automated tests are run as part of the maven build. Tests can be disabled using
standard failsafe options e.g. `-DskipTests`

### Running the application in a development environment

```
# run the backend using maven-exec-plugin (skip tests)
mvn package -DskipTests exec:exec@run-app
```

Application will be served on http://localhost:8080


#### Running the application in a production-like environment (TODO)

`docker-compose up` can be used to run a production-like environment
Alternatively, minikube can be used by following deployment steps specified below

## Deployment

There are some deployment scripts included with the project.
* '.gitlab-ci.yml' - docker build and deployment jobs
* `Dockerfile` - used to build a Docker image for use on a docker PaaS.
* `k8s_deploy.yml` - Kubernetes deployment manifest
* `docker-compose.yml` - For multi-container testing and deployment (TODO)

A production deployment is typically triggered through Gitlab CI, but it may help to explain the step-by-step process

1. Create a google cloud account
2. Initialize a project (rest-sorter) for google container engine (GKE)
3. Setup CI credentials
- In the GKE create a service account 'Gitlab CI' with 'Editor' role
- Furnish and export the private key as a JSON file
- For Gitlab CI setup: Load the JSON into project as a secret called 'GOOGLE_KEY'

4. Install gcloud command line tools and/or update components
- `gcloud components update`

5. Login using Oauth or activate service account key, either:
- `gcloud auth login` OR
- `gcloud auth activate-service-account --key-file key.json`

6. Set the project to the one created in step 2
- `gcloud config set project rest-sorter`

7. Create the GKE cluster, setup some parameters and connect to the cluster
- `gcloud container clusters create rest-sorter-cluster --zone us-west1-b --machine-type f1-micro`
- `gcloud config set compute/zone $GCP_REGION`
- `gcloud config set container/use_client_certificate True`
- `gcloud container clusters get-credentials rest-sorter-cluster`


8. Build a docker image if not done already, login to the public registry and push the built image
- `docker build -t ckendall/rest-sorter .`
- `docker login -u ckendall -p $REGISTRY_PASSWORD -e $REGISTRY_EMAIL`
- `docker push ckendall/rest-sorter`

9. Create a docker-registry secret that enables use of Gitlab CI registry. Note the env vars that must be set.
- `kubectl create secret docker-registry $REGISTRY_SERVER --docker-server=$REGISTRY_SERVER --docker-username=$REGISTRY_USERNAME --docker-password=$REGISTRY_PASSWORD --docker-email=$REGISTRY_EMAIL -o yaml --dry-run | kubectl replace -f -`

10. Use the deployment spec to deploy the application (will download docker image and deploy)
- `kubectl replace -f k8s_deploy.yml --record=true`

11. Expose the deployment to the internet through a load balancer service
- `kubectl expose deployment rest-sorter-deployment --type="LoadBalancer"`


### Coding style

No formatting/linting currently performed on the Java code

## Built With

In order to better understand the architecture behind the project.
It is recommended to read up on:
* [Vert.x](http://vertx.io) - a JVM based microservice framework

## Authors

* **Chris Kendall** - *Initial work* - [cwkendall](https://gitlab.com/cwkendall)

## Acknowledgments

* [12-factor applications](http://12factor.net) - inspiration for the architecture
* [Docker](https://docker.com) - container technology for relocatable applications
* [Kubernetes](https://kubernetes.io/) - Docker based PaaS developed and run by Google as GKE
