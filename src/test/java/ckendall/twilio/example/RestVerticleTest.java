package ckendall.twilio.example;

import com.fasterxml.jackson.annotation.JsonInclude;
import guru.nidi.ramltester.RamlDefinition;
import guru.nidi.ramltester.RamlLoaders;
import guru.nidi.ramltester.jaxrs.CheckingWebTarget;
import guru.nidi.ramltester.junit.RamlMatchers;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.*;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.Map;

import static guru.nidi.ramltester.junit.RamlMatchers.validates;

/**
 * @author chris on 30/6/17.
 */
@RunWith(VertxUnitRunner.class)
public class RestVerticleTest {

  private static final RamlDefinition api = RamlLoaders.fromClasspath()
          .load("/webroot/api.raml")
          .assumingBaseUri("http://localhost:8080");

  private ResteasyClient client = new ResteasyClientBuilder().build();

  private CheckingWebTarget checking;

  @BeforeClass
  public static void bootApp() {
    Future deployment = Future.future();
    Vertx vertx = Vertx.vertx();
    vertx.deployVerticle(RestVerticle.class.getName(), deployment.completer());
  }

  @Before
  public void createTarget() throws InterruptedException {
    //Thread.sleep(500);
    checking = api.createWebTarget(client.target("http://localhost:8080"));
  }

  public static class IntArray{
    public IntArray(){}
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Object id = null;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Object[] numbers = null;
  }

  @Test
  public void testApiValidation(TestContext tc){
    Assert.assertThat(api.validate(), validates());
  }

  @Test
  public void testDefaultUnsorted(TestContext tc) {
    IntArray test = new IntArray();
    test.numbers = new Integer[]{4,3,2,1};
    IntArray expected = new IntArray();
    expected.numbers = new Integer[]{4,3,2,1};

    Response resp = checking.path("api/arrays").request().post(Entity.json(test));
    IntArray output = resp.readEntity(IntArray.class);

    Object id = output.id;
    resp = checking.path("api/arrays/"+id.toString()).request().get();
    output = resp.readEntity(IntArray.class);
    Assert.assertArrayEquals(expected.numbers, output.numbers);
    Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void testSort(TestContext tc) {
    IntArray test = new IntArray();
    test.numbers = new Integer[]{4,3,2,1};
    IntArray expected = new IntArray();
    expected.numbers = new Integer[]{1,2,3,4};

    Response resp = checking.path("api/arrays").request().post(Entity.json(test));
    IntArray output = resp.readEntity(IntArray.class);

    Object id = output.id;
    resp = checking.path("api/arrays/"+id.toString())
            .queryParam("sortOrder", "asc").request().get();
    output = resp.readEntity(IntArray.class);
    Assert.assertArrayEquals(expected.numbers, output.numbers);
    Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void testReverseSort(TestContext tc) {
    IntArray test = new IntArray();
    test.numbers = new Integer[]{1,2,3,4};
    IntArray expected = new IntArray();
    expected.numbers = new Integer[]{4,3,2,1};

    Response resp = checking.path("api/arrays").request().post(Entity.json(test));
    IntArray output = resp.readEntity(IntArray.class);

    Object id = output.id;
    resp = checking.path("api/arrays/"+id.toString())
            .queryParam("sortOrder", "desc").request().get();
    output = resp.readEntity(IntArray.class);
    Assert.assertArrayEquals(expected.numbers, output.numbers);
    Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void testOverMaxInput(TestContext tc) {
    Integer[] testArray = new Integer[1001];
    IntArray test = new IntArray();
    for(int i = 0; i < testArray.length; i++){
      testArray[i] = i;
    }
    test.numbers = testArray;
    Response resp = checking.path("api/arrays").request().post(Entity.json(test));
    Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());
    Assert.assertEquals(413, resp.getStatus());
    Map output = resp.readEntity(Map.class);
    Assert.assertNotNull(output.get("error"));
  }


  @Test
  public void testBadFormat(TestContext tc) {
    IntArray test = new IntArray();
    test.numbers = new String[]{"4","3","2","aaa"};
    Response resp = checking.path("api/arrays").request().post(Entity.json(test));
    //Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());
    Assert.assertEquals(400, resp.getStatus());
    Map output = resp.readEntity(Map.class);
    Assert.assertNotNull(output.get("error"));
  }

  @Test
  public void testBadPath(TestContext tc) {
    IntArray test = new IntArray();
    test.numbers = new Integer[]{1,2,3,4};
    Response resp = checking.path("api/notarrays").request().post(Entity.json(test));
    //Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());
    Assert.assertEquals(404, resp.getStatus());
  }

  @Test
  public void testPage(TestContext tc) {
    IntArray test = new IntArray();
    test.numbers = new Integer[]{4,3,2,1};
    IntArray expected = new IntArray();
    expected.numbers = new Integer[]{1};

    Response resp = checking.path("api/arrays").request().post(Entity.json(test));
    IntArray output = resp.readEntity(IntArray.class);

    Object id = output.id;
    resp = checking.path("api/arrays/"+id.toString())
            .queryParam("sortOrder","asc")
            .queryParam("page","1")
            .queryParam("per_page", "1")
            .request().get();
    output = resp.readEntity(IntArray.class);
    Assert.assertArrayEquals(expected.numbers, output.numbers);
    Assert.assertThat(checking.getLastReport(), RamlMatchers.hasNoViolations());
  }

}
