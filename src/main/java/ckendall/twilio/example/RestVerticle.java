package ckendall.twilio.example;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author chris on 30/6/17.
 */
public class RestVerticle extends AbstractVerticle {

  Logger log = LoggerFactory.getLogger(RestVerticle.class);

  private static final int MAX_ARRAY_SIZE = 1000;
  private static final int DEFAULT_PAGE_SIZE = 100;
  private static final String ARRAYS_KEY = "arrays";

  @Override
  public void start() throws Exception {
    Router router = Router.router(vertx);
    Router apiRouter = Router.router(vertx);
    router.mountSubRouter("/api", apiRouter);

    apiRouter.route()
            .consumes("application/json")
            .produces("application/json")
            .handler(BodyHandler.create());
    apiRouter.post("/arrays").handler(rc -> {
      JsonObject obj = rc.getBodyAsJson();
      JsonArray items = obj.getJsonArray("numbers");
      if(items == null){
        rc.response().setStatusCode(400)
                .putHeader("content-type", "application/json")
                .end(new JsonObject().put("error", "input array of numbers not provided").encode());
      }
      else if(items.size() > MAX_ARRAY_SIZE){
        rc.response().setStatusCode(413)
                .putHeader("content-type", "application/json")
                .end(new JsonObject().put("error", "input array of numbers is too large, maximum "+MAX_ARRAY_SIZE+" elements").encode());
      }
      else {
        try {
          // write input data to backing store
          List output = items.stream().map(e -> (int) e).collect(Collectors.toList());
          vertx.sharedData().getCounter("index", counter -> {
            if(counter.succeeded()) {
              counter.result().getAndIncrement(id -> {
                if(id.succeeded()) {
                  Long nextIndex = id.result();
                  if (vertx.sharedData().getLocalMap(ARRAYS_KEY)
                          .putIfAbsent("items#" + nextIndex, new JsonArray(output).encode()) == null){
                    rc.response().setStatusCode(201)
                            .putHeader("content-type", "application/json")
                            .end(new JsonObject().put("id", nextIndex).encode());
                    // kick off sort job which will callback async
                    JsonObject asyncSort = obj.getJsonObject("sort");
                    if(asyncSort != null) {
                      String callbackUrl = asyncSort.getString("callback");
                      try {
                        URL url = new URL(callbackUrl);
                        vertx.executeBlocking(blocking -> {
                          String sortOrder = asyncSort.getString("order");
                          if (sortOrder != null) {
                            if ("asc".equalsIgnoreCase(sortOrder)) {
                              Collections.sort(output);
                            } else if ("desc".equalsIgnoreCase(sortOrder)) {
                              Collections.sort(output, Collections.reverseOrder());
                            }
                          }
                          blocking.complete(output);
                        }, false, callback -> {
                          HttpClient client = vertx.createHttpClient();
                          client.post(url.getPort(), url.getHost(), url.getPath())
                                  .handler(resp -> log.info("Callback resp: "+resp.statusCode()))
                                  .putHeader("content-type", "application/json")
                                  .end(new JsonObject().put("id", nextIndex).put("numbers", new JsonArray(output)).encode());
                        });
                      }  catch(MalformedURLException e){
                        rc.response().setStatusCode(400)
                                .putHeader("content-type", "application/json")
                                .end(new JsonObject().put("error","malformed callback url").encode());
                      }
                    }
                  } else {
                    rc.response().setStatusCode(409).end();
                  }
                } else {
                  rc.response().setStatusCode(400).end();
                }
              });
            } else {
              rc.response().setStatusCode(400).end();
            }
          });
          //Collections.sort(input.getList());
        } catch(ClassCastException e){
          rc.response().setStatusCode(400)
                  .putHeader("content-type", "application/json")
                  .end(new JsonObject().put("error","input array has non integral values").encode());
        }
      }
    });
    apiRouter.get("/arrays/:arrayId").handler(rc -> {
      String arrayId = rc.request().getParam("arrayId");
      String json = (String)vertx.sharedData().getLocalMap(ARRAYS_KEY).get("items#"+arrayId);
      if(json != null) {
        JsonArray array = new JsonArray(json);
        String sort = rc.request().params().get("sortOrder");
        String page = rc.request().params().get("page");
        String pageSize = rc.request().params().get("per_page");
        if(sort != null){
          if("asc".equalsIgnoreCase(sort)){
            Collections.sort(array.getList());
          }
          else if("desc".equalsIgnoreCase(sort)){
            Collections.sort(array.getList(), Collections.reverseOrder());
          }
        }
        if(page != null){
          Integer pageNum = Integer.parseInt(page);
          Integer pageSizeNum = pageSize != null ? Integer.parseInt(pageSize) : DEFAULT_PAGE_SIZE;
          array = new JsonArray(array.getList().subList((pageNum-1)*pageSizeNum,Math.min((pageNum)*pageSizeNum, array.getList().size())));
        }
        rc.response().setStatusCode(200)
                .putHeader("content-type", "application/json")
                .end(new JsonObject().put("numbers", array).encode());
      } else {
        rc.response().setStatusCode(404).end();
      }
    });
    router.route("/*").handler(StaticHandler.create());

    vertx.createHttpServer().requestHandler(router::accept).listen(8080);
  }

}
