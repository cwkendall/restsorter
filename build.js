var fs = require('fs');

if(!fs.existsSync('dist')){
    fs.mkdirSync('dist');
}

if(!fs.existsSync('dist/index.html')){
    const builder = require('api-console-builder');

    builder({
      dest: 'dist',
      raml: 'src/main/resources/webroot/api.raml',
      useJson: true,
      inlineJson: false,
      noOptimization: false
    })
    .then(() => console.log('Build complete'))
    .catch((cause) => console.log('Build error', cause.message));
}

const {RamlJsonGenerator} = require('raml-json-enhance-node');

const enhancer = new RamlJsonGenerator('src/main/resources/webroot/api.raml', {
  output: 'dist/api.json'
});

enhancer.generate()
.then((json) => {
  // The file is saved now.
  // And the JS object is available to use.
  console.log(json);
});
